#!/bin/bash

# PURPOSE: when I touch a middle-button at my headset directly connected
#          to Lenovo T530, I want to trigger a custom action (screen lock)

hook_on() {
	{ xscreensaver-command -restart || ( xscreensaver& ) ; true; }
	sleep 1
	xscreensaver-command -lock
}

hook_off() {
	true
}

status=0
trap 'status=1' USR1
while read line; do
	case "$line" in
	"card 0, #19 (0,0,0,Mic Jack,0) VALUE"*)
		#echo 0, $status
		[ $status -ne 0 ] || status=1;;
	"card 0, #13 (2,0,0,Mic Boost Volume,0) VALUE"*)
		#echo 2, $status
		[ $status -ne 1 ] || status=2
		[ $status -ne 20 ] || status=21;;
	"card 0, #15 (2,0,0,Internal Mic Boost Volume,0) VALUE"*)
		#echo 3, $status
		[ $status -ne 2 ] || status=3
		[ $status -ne 1 ] || status=20;;
	"card 0, #12 (2,0,0,Capture Switch,0) VALUE"*)
		#echo 4, $status
		[ $status -ne 4 ] || { hook_on; status=0; }
		[ $status -ne 3 ] || status=4
		[ $status -ne 21 ] || { hook_off; status=0; };;
	*)
		#echo 5
		status=0;;
	esac
	{ sleep 1; kill -USR1 $$; } &
done < <(script -qc 'alsactl monitor' /dev/null)
